const request = require('supertest');
const app = require('../index.js');


describe('GET /test', function(){
	it ('displays test', function(done){
		request(app).get('/test').expect("This is not a test", done);
	});
});