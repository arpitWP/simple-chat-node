var socket = io.connect('http://localhost:4500');

var message = document.getElementById('message'),
      handle = document.getElementById('handle'),
      btn = document.getElementById('send'),
    output = document.getElementById('output');


btn.addEventListener('click', function(){
  socket.emit('sendToManager', {
      message : message.value,
      hotelId : "5e90a18591667d3a86288127",
      roomNum : "D123"
  });
  output.innerHTML += '<p><strong> 789 : </strong>' + message.value + '</p>';
  message.value = "";
});

socket.emit('joinGuest',{
    hotelId : "5e90a18591667d3a86288127",
    room : "D123"
});


socket.on('receive', function(data){
    console.log("received");
    output.innerHTML += '<p><strong> Manager : </strong>' + data.message + '</p>';
});

