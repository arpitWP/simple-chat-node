var express = require('express');
var socket = require('socket.io');
var app = express();
app.use(express.static('./public'));
var port = 3000;
var server = app.listen(port, ()=>{
    console.log(`Listening on port 3000`)
})

var io = socket(server);

app.get('/test', (req,res)=>{
	res.send("This is a test");
});

io.on('connection', (socket)=>{
    console.log("Made new connection " + socket.id);
    socket.on('join', function(data){
        socket.join(data.user);
        socket.emit("welcome", {
            greeting : "Welcome to " + data.user + ". How may we help?",
            handle : "Manager"
        });
    });
});

module.exports = app;